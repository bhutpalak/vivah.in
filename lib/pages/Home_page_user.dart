import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:vivah/pages/User_bio_data.dart';
import 'package:vivah/Vivah_cards.dart';
import 'package:http/http.dart' as http;
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';


import '../api/rest_client2.dart';

class Home_page_user extends StatefulWidget {
  @override
  State<Home_page_user> createState() => _Home_page_userState();
}

class _Home_page_userState extends State<Home_page_user> {
  List<dynamic> users = [];
  List<dynamic> usersbiodata = [];
  int i = 1;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          shadowColor: Colors.black,
          backgroundColor: Color.fromRGBO(255, 67, 101, 1),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Center(
              child: Image.asset(
                "assets/img/vivah_logo.jpg",
                height: 5.h,
                alignment: Alignment.center,
              ),
            ),
          ),
        ),
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        body: Center(
          child: FutureBuilder<List<dynamic>>(
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Container(
                  child: Stack(
                    children: getUserViews(snapshot.data!),
                  ),
                );
              } else {
                return CircularProgressIndicator(
                  color: Colors.white,
                );
              }
            },
            future: getUser(),
          ),
        ),
      );
    });
  }

  List<Widget> getUserViews(List<dynamic> data) {
    List<Widget> viewList = [];
    for (int i = 0; i < data.length; i++) {
      viewList.add(Vivah_cards(
        users: data[i],
      ));
    }
    return viewList;
  }

  List<Widget> getUserBiodata(List<dynamic> data) {
    List<Widget> viewListBiodata = [];
    for (int i = 0; i < data.length; i++) {
      viewListBiodata.add(User_bio_data(
        usersbiodata: data[i],
      ));
    }
    return viewListBiodata;
  }

  // api call in simple http
  // Future<List<dynamic>> fetchusers() async {
  //   print("Fetch user called!");
  //   const url =
  //       "https://63bc18b3fa38d30d85bb7c6c.mockapi.io/api/user_data/user_data";
  //   final uri = Uri.parse(url);
  //   final response = await http.get(uri);
  //   final body = response.body;
  //   print("fetchuser completed! ${body.toString()}");
  //   final List<dynamic> json = jsonDecode(body.toString());
  //   users = json;
  //   usersbiodata = json;
  //   return json;
  // }

  // apio call in retrofit
  Future<List<dynamic>> getUser() async {
    print("Fetch user called!");
    final dio = Dio();
    final client = RestClient(dio);
    final List<dynamic> json = jsonDecode(await client.getTasks());
    print(json.toString());
    return json;
  }

//   Future<List<dynamic>> getUsers() async {
//     print("getUser called  1");
//     final dio = Dio();
//     print("getUser called  2");
//     final client = RestClient(dio);
//     print("getUser called  3");
//     List<dynamic> Data = client.getUsers().toString() as List;
//     print("getUser called  4");
//     print(Data);
//     return Data;
//   }
// }

// //Future<Map<String,dynamic>> callapi() async {
//   final res = await http.get(Uri.parse(
//       "https://63bc18b3fa38d30d85bb7c6c.mockapi.io/api/user_data/user_data"));
//   print(res.body.toString());
//   Map<String,dynamic> map = json.decode(res.body.toString());
//   print("DATA:::$map");
//   return map;
// }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:vivah/utils/usermodel.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/routes.dart';

class User_profile extends StatefulWidget {
  @override
  State<User_profile> createState() => _User_profileState();
}

class _User_profileState extends State<User_profile> {
  User? user = FirebaseAuth.instance.currentUser;
  usermodel LoggedInuser = usermodel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseFirestore.instance.collection("users").doc(user!.uid).get().then(
      (value) {
        this.LoggedInuser = usermodel.fromMap(value.data());
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          elevation: 0,
          shadowColor: Colors.black,
          backgroundColor: Color.fromRGBO(255, 67, 101, 1),
          title: Container(
              padding: EdgeInsets.only(top: 20.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/img/setting.png",
                    height: 4.h,
                    alignment: Alignment.topRight,
                  ),
                  Text("   Settings",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.sp,
                          fontFamily: "Comfortaa"),
                      textAlign: TextAlign.left),
                ],
              )),
        ),
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Color.fromRGBO(255, 67, 101, 1), Colors.white],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0.1, 1])),
            ),
            Column(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(5.w, 5.h, 0, 0),
                  height: 15.h,
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(100.sp),
                        child: Container(
                            height: 10.h,
                            width: 20.w,
                            child: Image.asset(
                              "assets/img/pooja.jpeg",
                              fit: BoxFit.cover,
                            )),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "${LoggedInuser.firstname} ${LoggedInuser.lastname}",
                                style: TextStyle(
                                    fontSize: 18.5.sp,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Comfortaa"),
                              )),
                          Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.fromLTRB(5.w, 1.5.h, 0, 0),
                              child: Text(
                                "${LoggedInuser.email} ",
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.sp,
                                    fontFamily: "Comfortaa"),
                              )),
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.fromLTRB(4.w, 2.h, 3.w, 1.h),
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/edit.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Edit Profile",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/bell.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Notification",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/star.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Upgrade to Vivah+",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/policy.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Privacy Policy",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/help-desk.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Help Center",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.sp),
                    color: Colors.black38,
                  ),
                  height: 8.h,
                  margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
                  child: InkWell(
                    onTap: () => logout(),
                    child: Row(
                      children: [
                        Container(
                            margin: EdgeInsets.fromLTRB(5.w, 2.5.h, 2.w, 2.5.h),
                            child: Image.asset("assets/img/logout.png")),
                        Container(
                          padding: EdgeInsets.only(left: 1.w),
                          child: Text(
                            "Logout",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.5.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }

  Future<void> logout() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushNamed(context, Myroutes.homeRoute);
  }
}

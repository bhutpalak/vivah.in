import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:vivah/utils/routes.dart';

class User_home extends StatelessWidget {
  final List<String> imgList = [
    'assets/img/list 1.jpg',
    'assets/img/list 2.jpg',
    'assets/img/list 3.jpg',
    'assets/img/list 4.jpg',
    'assets/img/list 5.jpg',
    'assets/img/list 6.jpg',
    'assets/img/list 7.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          shadowColor: Colors.black,
          backgroundColor: Color.fromRGBO(255, 67, 101, 1),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Center(
              child: Image.asset(
                "assets/img/vivah_logo.jpg",
                height: 5.h,
                alignment: Alignment.center,
              ),
            ),
          ),
        ),
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 2.h,
              ),
              Container(
                height: 55.h,
                child: CarouselSlider(
                  items: imgList
                      .map(
                        (item) => ClipRRect(
                          borderRadius: BorderRadius.circular(20.sp),
                          child: Center(
                            // child: Image.network(
                            //   item,
                            //   fit: BoxFit.cover,
                            //   height: double.infinity,
                            // ),
                            child: Image.asset(
                              item,
                              fit: BoxFit.cover,
                              height: double.infinity,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                  options: CarouselOptions(
                      height: double.infinity,
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: true),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(10.w, 5.h, 0, 0),
                //decoration: BoxDecoration(color: Colors.red),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Find !",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25.sp,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Com"),
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    Row(
                      children: [
                        Text(
                          "your",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.sp,
                              fontFamily: "Com"),
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Text(
                          "Future Partner",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Com"),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    Row(
                      children: [
                        Text(
                          "with",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.sp,
                              fontFamily: "Com"),
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Text(
                          "Us",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Com"),
                        ),
                        SizedBox(
                          width: 3.w,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 0.5.h),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(40.sp),
                              /////////////////// box shadow ////////////////////
                              boxShadow: [
                                // darker on bottom right
                                BoxShadow(
                                    color: Colors.black,
                                    offset: Offset(1, 1),
                                    blurStyle: BlurStyle.normal,
                                    blurRadius: 10,
                                    spreadRadius: 0),

                                // // lighter on bottom right
                                // BoxShadow(
                                //     color: Colors.white,
                                //     offset: Offset(-1, -1),
                                //     blurRadius: 0,
                                //     spreadRadius: 0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              hoverColor: Colors.red,
                              highlightColor: Colors.grey,
                              onTap: () {
                                Navigator.pushNamed(
                                    context, Myroutes.user_main_home);
                              },
                              child: Container(
                                height: 6.h,
                                width: 45.w,
                                child: Center(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Continue ",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "Com"),
                                    ),
                                    Icon(Icons.arrow_right),
                                  ],
                                )),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}

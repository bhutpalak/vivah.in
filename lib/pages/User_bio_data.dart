import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class User_bio_data extends StatelessWidget {
  dynamic usersbiodata;

  User_bio_data({this.usersbiodata});

  int count = 0;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, screenType) {
        return Scaffold(
          extendBodyBehindAppBar: true,
          extendBody: true,
          backgroundColor: Color.fromRGBO(255, 67, 101, 1),
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              radius: 100,
              child: Container(
                margin: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 67, 101, 1),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Icon(Icons.arrow_back_sharp, size: 19.sp),
              ),
            ),
          ),
          body: Container(
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  Container(
                    child: Image.network(
                      "${usersbiodata!['image']}",
                      height: 30.h,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(height: 25.h),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15.sp)),
                        margin: EdgeInsets.symmetric(horizontal: 14.sp),
                        height: 18.h,
                        width: 100.w,
                        // child: Text("${usersbiodata!['first_name']}",
                        //     style: TextStyle(height: 10)),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                padding: EdgeInsets.all(15.sp),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10.sp),
                                  child: Image.network(
                                    "${usersbiodata!['image']}",
                                    height: 30.h,
                                    width: double.infinity,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(left: 15.sp),
                                      child: Text(
                                        "${usersbiodata!['first_name']}",
                                        style: TextStyle(
                                            fontSize: 20.sp,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "Com"),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: EdgeInsets.only(left: 15.sp),
                                      child: Text(
                                        "${usersbiodata!['about_user']}",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            color: Colors.grey,
                                            fontFamily: "Com"),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      alignment: Alignment.topLeft,
                                      padding: EdgeInsets.only(left: 15.sp),
                                      child: Text(
                                        "${usersbiodata!['email']}",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "Com"),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 5.h),
                      Column(
                        children: [
                          SingleChildScrollView(
                            child: Container(
                              height: 108.sp,
                              margin: EdgeInsets.symmetric(horizontal: 14.sp),
                              width: 100.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.sp),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 20.sp,
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 6.sp,
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "Personal Details",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontFamily: "Com",
                                                fontSize: 19.sp),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom:
                                                BorderSide(color: Colors.grey),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 18.sp,
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Name : ${usersbiodata!['first_name']} ${usersbiodata!['last_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Date Of Birth : ${usersbiodata!['dob']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Birth Place : ${usersbiodata!['dob_place']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Caste/Gotra : ${usersbiodata!['caste']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Marital Status : ${usersbiodata!['material_status']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Education : ${usersbiodata!['education']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Occupation : ${usersbiodata!['occupasion']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Job/Business : ${usersbiodata!['job']} ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Salary/Income : ${usersbiodata!['salary']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Address : ${usersbiodata!['address']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                          ),
                          SizedBox(height: 3.h),
                          SingleChildScrollView(
                            child: Container(
                              height: 85.sp,
                              margin: EdgeInsets.symmetric(horizontal: 14.sp),
                              width: 100.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.sp),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 20.sp,
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 6.sp,
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "Family Details",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontFamily: "Com",
                                                fontSize: 19.sp),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom:
                                                BorderSide(color: Colors.grey),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 18.sp,
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Father's Name : ${usersbiodata!['father_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Mother's Name  : ${usersbiodata!['mother_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Sister's Name  : ${usersbiodata!['sister_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Brother's Name  : ${usersbiodata!['brother_name']} ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Grand Father's Name : ${usersbiodata!['grandfather_name']} ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Grand Mother's Name : ${usersbiodata!['grandmother_name']} ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                          ),
                          SizedBox(height: 3.h),
                          SingleChildScrollView(
                            child: Container(
                              height: 64.sp,
                              margin: EdgeInsets.symmetric(horizontal: 14.sp),
                              width: 100.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.sp),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 20.sp,
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 6.sp,
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "Maternal Details",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontFamily: "Com",
                                                fontSize: 19.sp),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom:
                                                BorderSide(color: Colors.grey),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 18.sp,
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Mamaji  : ${usersbiodata!['mamaji_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Naniji  : ${usersbiodata!['nanaji_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Nanaji  : ${usersbiodata!['naniji_name']}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Com",
                                              fontSize: 17.sp),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                          ),
                          SizedBox(height: 3.h),
                          SingleChildScrollView(
                            child: Container(
                              height: 70.sp,
                              margin: EdgeInsets.symmetric(horizontal: 14.sp),
                              width: 100.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.sp),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 20.sp,
                                ),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 6.sp,
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "Contact Details",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontFamily: "Com",
                                                fontSize: 19.sp),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom:
                                                BorderSide(color: Colors.grey),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 18.sp,
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(Icons.home_filled, size: 18),
                                            Text(
                                              " :  ${usersbiodata!['address']} ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: "Com",
                                                  fontSize: 17.sp),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(Icons.call, size: 18),
                                            Text(
                                              " :  ${usersbiodata!['contact_number']}  ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: "Com",
                                                  fontSize: 17.sp),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(Icons.whatsapp, size: 18),
                                            Text(
                                              " :  ${usersbiodata!['whatsapp_number']}  ",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: "Com",
                                                  fontSize: 17.sp),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(Icons.email, size: 18),
                                            Text(
                                              " :  ${usersbiodata!['email']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: "Com",
                                                  fontSize: 17.sp),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                          ),
                          SizedBox(height: 5.h),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

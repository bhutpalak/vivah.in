import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:vivah/Main_home.dart';
import 'package:vivah/utils/routes.dart';

class Login extends StatefulWidget {
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  String username = "";

  String display_username = "";
  bool crossButton = false;
  bool changeButton = false;

  bool isrememberme = false;
  bool passshow = true;

  final _formKey = GlobalKey<FormState>();

  void moveToHome(String username, String password) async {
    if (_formKey.currentState!.validate()) {
      await Future.delayed(Duration(seconds: 0));
      await _auth
          .signInWithEmailAndPassword(email: username, password: password)
          .then((uid) => {
                setState(() {
                  changeButton = true;
                }),
                Fluttertoast.showToast(msg: "Login Succesful"),
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => Main_page()),
                ),
                setState(() {
                  changeButton = false;
                })
              })
          .catchError(
        (e) {
          setState(() {
            crossButton = true;
          });
          Timer(Duration(seconds: 2), () {
            setState(() {
              crossButton = false;
            });
          });

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Processing Data')),
          );
          Fluttertoast.showToast(msg: e!.message);
        },
      );
      // await Navigator.pushNamed(context, Myroutes.user_homeRoute);
      // setState(() {
      //   changeButton = false;
      // });
    }
  }

  // firebase
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
          extendBodyBehindAppBar: true,
          extendBody: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              radius: 100,
              child: Container(
                margin: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  // color: Color.fromRGBO(255, 67, 101, 1),
                  color: Color.fromRGBO(255, 67, 101, 1),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Icon(Icons.arrow_back_sharp,
                    color: Colors.white, size: 19.sp),
              ),
            ),
          ),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                "assets/img/couple_eyes.jpg",
                fit: BoxFit.cover,
              ),
              Container(
                color: Colors.white.withOpacity(0.1),
              ),
              Column(
                children: [
                  // Expanded(
                  //   child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),),
                  //   flex: 1,
                  // ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(),
                            height: 48.h,
                          ),
                          Container(
                            // padding: EdgeInsets.all(10),
                            padding: EdgeInsets.symmetric(
                                vertical: 0.h, horizontal: 2.w),
                            child: Card(
                              shape: ContinuousRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.sp)),
                              color: Colors.white,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.fromLTRB(6.w, 3.h, 5.w, 0.h),
                                    child: Text(
                                      "Welcome Back, $username",
                                      style: TextStyle(
                                          fontSize: 22.sp,
                                          fontFamily: "Satisfy"),
                                    ),
                                  ),
                                  // Text(
                                  //   "Welcome Back, $username",
                                  //   style: TextStyle(
                                  //       fontSize: 25, fontFamily: "Satisfy"),
                                  // ),
                                  Container(
                                    child: Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(4.w, 0.h, 4.w, 0),
                                      // padding: const EdgeInsets.fromLTRB(
                                      //     20, 10, 20, 0),
                                      child: Form(
                                        key: _formKey,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                    child: Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      0, 0, 1.5.w, 0),
                                                  child: Image.asset(
                                                      "assets/img/user.png"),
                                                )),
                                                Expanded(
                                                  child: TextFormField(
                                                    controller:
                                                        usernameController,
                                                    keyboardType:
                                                        TextInputType.name,
                                                    onSaved: (newValue) {
                                                      usernameController.text =
                                                          newValue!;
                                                    },
                                                    textInputAction:
                                                        TextInputAction.next,
                                                    style: TextStyle(
                                                      fontFamily: "Com",
                                                      fontSize: 19.sp,
                                                    ),
                                                    cursorColor: Colors.grey,
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              2.w, 2.h, 0, 3.h),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .black)),
                                                      floatingLabelStyle:
                                                          TextStyle(
                                                        color: Color.fromRGBO(
                                                            255, 67, 101, 1),
                                                        fontFamily: "Com",
                                                        fontSize: 17.sp,
                                                      ),
                                                      hintText: "Enter Email",
                                                      hintStyle: TextStyle(
                                                          color: Colors.grey,
                                                          fontFamily: "Com",
                                                          fontSize: 16.sp),
                                                      labelText: "Email",
                                                      labelStyle: TextStyle(
                                                          color: Colors.grey,
                                                          fontFamily: "Com",
                                                          fontSize: 18.sp),
                                                    ),
                                                    onChanged: (value) {
                                                      username = value;
                                                      while (value == '@') {
                                                        display_username =
                                                            value;
                                                      }
                                                      setState(() {});
                                                    },
                                                    validator: (value) {
                                                      if (value != null &&
                                                          value.isEmpty) {
                                                        return "Email cannot be Empty";
                                                      } else if (value !=
                                                              null &&
                                                          value.length < 6) {
                                                        return "Email length should be atleast 6";
                                                      }
                                                      return null;
                                                    },
                                                  ),
                                                  flex: 13,
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                    child: Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      0, 0, 1.5.w, 0),
                                                  child: Image.asset(
                                                      "assets/img/padlock.png"),
                                                )),
                                                Expanded(
                                                  child: TextFormField(
                                                    controller:
                                                        passwordController,
                                                    onSaved: (newValue) {
                                                      passwordController.text =
                                                          newValue!;
                                                    },
                                                    textInputAction:
                                                        TextInputAction.next,
                                                    obscureText: passshow,
                                                    style: TextStyle(
                                                        fontSize: 20.sp),
                                                    cursorColor: Colors.grey,
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.fromLTRB(
                                                              2.w, 1.h, 0, 3.h),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .black)),
                                                      floatingLabelStyle:
                                                          TextStyle(
                                                        color: Color.fromRGBO(
                                                            255, 67, 101, 1),
                                                        fontFamily: "Com",
                                                        fontSize: 17.sp,
                                                      ),
                                                      hintText:
                                                          "Enter Password",
                                                      labelText: "Password",
                                                      hintStyle: TextStyle(
                                                          color: Colors.grey,
                                                          fontFamily: "Com",
                                                          fontSize: 16.sp),
                                                      labelStyle: TextStyle(
                                                          color: Colors.grey,
                                                          fontFamily: "Com",
                                                          fontSize: 18.sp),
                                                    ),
                                                    validator: (value) {
                                                      if (value != null &&
                                                          value.isEmpty) {
                                                        return "Password cannot be Empty";
                                                      } else if (value !=
                                                              null &&
                                                          value.length < 8) {
                                                        return "Password length should be atleast 8";
                                                      }
                                                      return null;
                                                    },
                                                  ),
                                                  flex: 12,
                                                ),
                                                Expanded(
                                                  child: InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        passshow = !passshow;
                                                      });
                                                    },
                                                    onDoubleTap: () {
                                                      setState(() {
                                                        passshow = true;
                                                      });
                                                    },
                                                    child: Container(
                                                        child: passshow
                                                            ? Image.asset(
                                                                "assets/img/view.png")
                                                            : Image.asset(
                                                                "assets/img/blind.png")),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              isrememberme = !isrememberme;
                                            });
                                          },
                                          child: Align(
                                            alignment: Alignment.center,
                                            child: Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 3.h),
                                              padding:
                                                  EdgeInsets.only(left: 2.w),
                                              child: Row(
                                                children: [
                                                  Checkbox(
                                                    checkColor: Colors.white,
                                                    activeColor: Color.fromRGBO(
                                                        255, 67, 101, 1),
                                                    onChanged: (value) {},
                                                    value: isrememberme,
                                                  ),
                                                  Text("Remember me",
                                                      style: TextStyle(
                                                          fontFamily: "Com",
                                                          fontSize: 16.sp)),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.pushNamed(
                                                context, Myroutes.forgot_pass);
                                          },
                                          child: Align(
                                            alignment: Alignment.center,
                                            child: Container(
                                                child: Text(
                                              "Forgot Password?",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      255, 67, 101, 1),
                                                  fontFamily: "Com",
                                                  fontSize: 15.5.sp),
                                            )),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 67, 101, 1),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: InkWell(
                              onTap: () => moveToHome(usernameController.text,
                                  passwordController.text),
                              child: AnimatedContainer(
                                duration: Duration(seconds: 1),
                                alignment: Alignment.center,
                                child: changeButton
                                    ? Icon(
                                        Icons.done,
                                        color: Colors.white,
                                      )
                                    : crossButton
                                        ? Icon(
                                            Icons.close,
                                            color: Colors.white,
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              "Login",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.sp,
                                                  fontFamily: "Com"),
                                            ),
                                          ),
                              ),
                            ),
                          ),
                          Container(
                            height: 100,
                          )
                        ],
                      ),
                    ),
                    flex: 1,
                  ),

                  //Expanded(child: Container())
                ],
              )
            ],
          ));
    });
  }
}

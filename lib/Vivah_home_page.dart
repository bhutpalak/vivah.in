import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:vivah/firebase_auth_methods.dart';
import 'package:vivah/utils/routes.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class Vivah_home_page extends StatelessWidget {
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, screenType) {
        return Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                "assets/img/couple_bg.jpg",
                fit: BoxFit.cover,
              ),
              Container(
                decoration: BoxDecoration(color: Colors.grey.withOpacity(0.1)),
              ),
              Column(
                children: [
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(height: 63.h, color: Colors.transparent),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 4.w),
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.w, vertical: 1.5.h),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.sp),
                              color: Colors.black54.withOpacity(0.7)
                              // boxShadow: [
                              //   BoxShadow(
                              //     color: Colors.white60,
                              //     blurRadius: 5.0,
                              //     spreadRadius: 1.0,
                              //   )
                              // ],
                              ),
                          child: Column(
                            children: [
                              // ClipRRect(
                              //   borderRadius: BorderRadius.circular(10),
                              //   child: Image.asset(
                              //     "assets/img/vivah_logo.jpg",
                              //     fit: BoxFit.cover,
                              //     height: 4.h,
                              //   ),
                              // ),
                              InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, Myroutes.signupRoute);
                                },
                                child: Container(
                                  margin: EdgeInsets.only(top: 0.6.h),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.h, horizontal: 6.w),
                                  decoration: BoxDecoration(
                                    color: Colors.white60.withOpacity(0.9),
                                    borderRadius: BorderRadius.circular(16.sp),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 0.1.w),
                                        child: Image.asset(
                                          "assets/img/phone_icon.png",
                                          height: 5.h,
                                        ),
                                      ),
                                      Text(
                                        "   Sign up with Email",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "Com"),
                                      ),
                                    ],
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  firebase_auth_methods(FirebaseAuth.instance)
                                      .sign_in_google(context)
                                      .then(
                                    (value) {
                                      Navigator.pushNamed(
                                          context, Myroutes.user_main_home);
                                    },
                                  );
                                  // Navigator.pushNamed(
                                  //     context, Myroutes.user_main_home);
                                },
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 2.h, 0, 1.h),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.h, horizontal: 6.w),
                                  decoration: BoxDecoration(
                                    //   color: Color.fromRGBO(255, 67, 101, 1),
                                    color: Colors.white60.withOpacity(0.9),
                                    borderRadius: BorderRadius.circular(16.sp),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 1.w),
                                        child: Image.asset(
                                          "assets/img/google_icon.png",
                                          height: 5.h,
                                        ),
                                      ),
                                      Text(
                                        " Sign up with Google",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "Com"),
                                      ),
                                    ],
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ),
                              // InkWell(
                              //   onTap: () {
                              //     firebase_auth_methods(FirebaseAuth.instance)
                              //         .sign_in_facebook(context)
                              //         .then(
                              //       (value) {
                              //         Navigator.pushNamed(
                              //             context, Myroutes.personal_details);
                              //       },
                              //     );
                              //   },
                              //   child: Container(
                              //     margin: EdgeInsets.fromLTRB(0, 2.h, 0, 0.9.h),
                              //     padding: EdgeInsets.symmetric(
                              //         vertical: 1.h, horizontal: 6.w),
                              //     decoration: BoxDecoration(
                              //       color: Colors.white60.withOpacity(0.9),
                              //       borderRadius: BorderRadius.circular(16.sp),
                              //     ),
                              //     child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       children: [
                              //         Container(
                              //           margin: EdgeInsets.only(right: 1.w),
                              //           child: Image.asset(
                              //             "assets/img/facebook_icon.png",
                              //             height: 5.h,
                              //           ),
                              //         ),
                              //         Text(
                              //           "Sign up with Facebook",
                              //           style: TextStyle(
                              //               fontSize: 16.sp,
                              //               fontWeight: FontWeight.bold,
                              //               fontFamily: "Com"),
                              //         ),
                              //       ],
                              //     ),
                              //     alignment: Alignment.center,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 1.h),
                          child: Text(
                              "-------------------   or   -------------------",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontFamily: "Com",
                                  color: Colors.white)),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 1.h, horizontal: 0),
                          margin: EdgeInsets.fromLTRB(4.w, 1.h, 4.w, 1.h),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.sp),
                            color: Color.fromRGBO(255, 67, 101, 1),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  "Already have an account?",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15.5.sp,
                                      fontFamily: "Com"),
                                  textAlign: TextAlign.right,
                                ),
                              ),
                              Container(width: 3.w),
                              Center(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, Myroutes.loginRoute);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.black26,
                                        borderRadius:
                                            BorderRadius.circular(12.sp)),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 1.h, horizontal: 3.w),
                                    margin: EdgeInsets.symmetric(
                                      vertical: 1.h,
                                    ),
                                    child: Text(
                                      "Login",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Com",
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ))
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

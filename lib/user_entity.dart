import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class user_entity {
  @JsonKey(name: 'first_name')
  final String First_name;
  @JsonKey(name: 'id')
  final String Id;
  @JsonKey(name: 'last_name')
  final String Last_name;
  @JsonKey(name: 'age')
  final String Age;
  @JsonKey(name: 'about_user')
  final String About_user;
  @JsonKey(name: 'image')
  final String Image;
  @JsonKey(name: 'email')
  final String Email;

  user_entity(
      {required this.First_name,
      required this.Id,
      required this.Last_name,
      required this.Age,
      required this.About_user,
      required this.Image,
      required this.Email});
}

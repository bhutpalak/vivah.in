// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<User?>? userFromJson(String str) => json.decode(str) == null
    ? []
    : List<User?>.from(json.decode(str)!.map((x) => User.fromJson(x)));

String userToJson(List<User?>? data) => json.encode(
    data == null ? [] : List<dynamic>.from(data!.map((x) => x!.toJson())));

class User {
  User({
    this.image,
    this.firstName,
    this.lastName,
    this.age,
    this.aboutUser,
    this.id,
  });

  String? image;
  String? firstName;
  String? lastName;
  int? age;
  String? aboutUser;
  String? id;

  factory User.fromJson(Map<String, dynamic> json) => User(
        image: json["image"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        age: json["age"],
        aboutUser: json["about_user"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
        "first_name": firstName,
        "last_name": lastName,
        "age": age,
        "about_user": aboutUser,
        "id": id,
      };
}

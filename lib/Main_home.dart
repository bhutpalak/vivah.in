import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:vivah/pages/Home_page_user.dart';
import 'package:vivah/pages/User_chat.dart';
import 'package:vivah/pages/User_home.dart';
import 'package:vivah/pages/User_profile.dart';
import 'package:vivah/pages/User_search.dart';

class Main_page extends StatefulWidget {
  @override
  State<Main_page> createState() => _Main_pageState();
}

class _Main_pageState extends State<Main_page> {
  final navigationKey = GlobalKey<CurvedNavigationBarState>();
  int index = 1;
  final screens = [
    User_home(),
    Home_page_user(),
    User_profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),

        body: screens[index],
        //Center(child: Text("hello"),),
        bottomNavigationBar: CurvedNavigationBar(
          onTap: (index) => setState(() {
            this.index = index;
          }),
          key: navigationKey,
          backgroundColor: Colors.transparent,
          height: 50,
          animationDuration: Duration(milliseconds: 200),
          buttonBackgroundColor: Color.fromRGBO(255, 67, 101, 1),
          color: Colors.white,
          index: index,
          items: [
            Icon(Icons.home, size: 30),
            Icon(Icons.favorite, size: 30),
            Icon(Icons.settings, size: 30),
          ],
        ),
      ),
    );
  }
}

// // back to home page
// key: navigationKey,onPressed: (){final navigationState.setpage
// (0);
// }

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:vivah/api/rest_client2.dart';
import 'package:vivah/utils/routes.dart';

class contact_details extends StatefulWidget {
  String? name;
  String? dob;
  String? dob_place;
  String? caste;
  String? maternal_status;
  String? education;
  String? occupasion;
  String? job;
  String? salary;
  String? address;
  String? father_name;
  String? mother_name;
  String? sister_name;
  String? brother_name;
  String? grandfather_name;
  String? grandmother_name;
  String? mamaji_name;
  String? nanaji_name;
  String? naniji_name;

  contact_details(
      {Key? key,
      required this.name,
      required this.dob,
      required this.dob_place,
      required this.caste,
      required this.maternal_status,
      required this.education,
      required this.occupasion,
      required this.job,
      required this.salary,
      required this.address,
      required this.father_name,
      required this.mother_name,
      required this.sister_name,
      required this.brother_name,
      required this.grandfather_name,
      required this.grandmother_name,
      required this.mamaji_name,
      required this.nanaji_name,
      required this.naniji_name})
      : super(key: key);

  @override
  State<contact_details> createState() => _contact_details();
}

class _contact_details extends State<contact_details> {
  final _formKey = GlobalKey<FormState>();
  final contact_number = new TextEditingController();
  final whatsapp_number = new TextEditingController();
  final email = new TextEditingController();

  final RoundedLoadingButtonController btn1 = RoundedLoadingButtonController();

  bool changebtn = false;
  bool nested_changebtn = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            radius: 100,
            child: Container(
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 67, 101, 1),
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(Icons.arrow_back_sharp, size: 19.sp),
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 4.h,
                    width: double.infinity,
                    margin:
                        EdgeInsets.symmetric(horizontal: 6.w, vertical: 1.5.h),
                    child: Text(
                      "Enter Your Maternal Details Here.",
                      style: TextStyle(fontSize: 18.sp, fontFamily: "Com"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    margin: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15.sp)),
                    child: Column(
                      children: [
                        Container(
                          height: 4.h,
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          width: double.infinity,
                          margin: EdgeInsets.fromLTRB(0.w, 3.h, 0.w, 0.h),
                          child: Text(
                            "Maternal Details",
                            style: TextStyle(
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Com"),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                // birth date

                                TextFormField(
                                  controller: contact_number,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    contact_number.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Contact Number",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Contact Number",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Contact Number cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Contact Number length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // dob place
                                TextFormField(
                                  controller: whatsapp_number,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    whatsapp_number.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Whatsapp Number",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: "  Whatsapp Number",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Whatsapp Number cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Whatsapp Number length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // email
                                TextFormField(
                                  controller: email,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    email.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Email",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: "  Email",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Email cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 6) {
                                      return "Username length should be atleast 6";
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // next page button

                  // Container(
                  //   height: 8.h,
                  //   width: double.infinity,
                  //   margin: EdgeInsets.fromLTRB(5.w, 0, 5.w, 3.h),
                  //   child: RoundedLoadingButton(
                  //       valueColor: Colors.black,
                  //       borderRadius: 12,
                  //       width: double.infinity,
                  //       color: Colors.white,
                  //       controller: btn1,
                  //       successColor: Colors.green,
                  //       errorColor: Colors.red,
                  //       successIcon: Icons.check,
                  //       onPressed: () {
                  //         // Navigator.pushNamed(context, Myroutes.family_details);
                  //         // Timer(Duration(seconds: 2), () {
                  //         //   btn1.reset();
                  //         // });
                  //         if (_formKey.currentState!.validate()) {
                  //           Navigator.pushNamed(
                  //               context, Myroutes.family_details);
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         } else {
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         }
                  //       },
                  //       child: Icon(
                  //         Icons.arrow_right_alt,
                  //         color: Colors.black,
                  //         shadows: [Shadow(color: Colors.black)],
                  //         size: 25.sp,
                  //       )),
                  // )

                  InkWell(
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        // btn change logic
                        getUser();
                        Timer(Duration(milliseconds: 500), () {
                          setState(() {
                            changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });

                        Future.delayed(
                          Duration(seconds: 2),
                          () {
                            Navigator.pushNamed(
                                context, Myroutes.congrats_signup);
                          },
                        );
                      } else {
                        Timer(Duration(milliseconds: 100), () {
                          setState(() {
                            nested_changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text(
                              'Enter Valid Data !',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontFamily: "Com"),
                            ),
                          ),
                        );
                      }
                    },
                    child: Container(
                        alignment: Alignment.center,
                        height: 7.h,
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(5.w, 1.h, 5.w, 4.h),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.sp),
                        ),
                        child: changebtn
                            ? Icon(
                                Icons.done,
                                size: 5.h,
                              )
                            : nested_changebtn
                                ? Icon(
                                    Icons.close,
                                    size: 5.h,
                                  )
                                : Icon(
                                    Icons.arrow_right_alt,
                                    size: 5.h,
                                  )),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  Future<void> getUser() async {
    print("Fetch user called!");
    final dio = Dio();
    final client = RestClient(dio);
    await client
        .addUser(
            widget.name,
            widget.dob,
            widget.dob_place,
            widget.caste,
            widget.maternal_status,
            widget.education,
            widget.occupasion,
            widget.job,
            widget.salary,
            widget.address,
            widget.father_name,
            widget.mother_name,
            widget.sister_name,
            widget.brother_name,
            widget.grandmother_name,
            widget.grandfather_name,
            widget.mamaji_name,
            widget.nanaji_name,
            widget.naniji_name,
            contact_number.text,
            whatsapp_number.text,
            email.text)
        .then(
      (value) {
        print("RESPONCE:: $value");
      },
    );
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:vivah/api/rest_client2.dart';
import 'package:vivah/signup_pages/family_details.dart';
import 'package:vivah/utils/routes.dart';

class personal_details extends StatefulWidget {
  @override
  State<personal_details> createState() => _personal_detailsState();
}

class _personal_detailsState extends State<personal_details> {
  final _formKey = GlobalKey<FormState>();
  final name = new TextEditingController();
  final dob = new TextEditingController();
  final dob_place = new TextEditingController();
  final caste = new TextEditingController();
  final maternal_status = new TextEditingController();
  final education = new TextEditingController();
  final occupasion = new TextEditingController();
  final job = new TextEditingController();
  final salary = new TextEditingController();
  final address = new TextEditingController();

  final RoundedLoadingButtonController btn1 = RoundedLoadingButtonController();

  bool changebtn = false;
  bool nested_changebtn = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            radius: 100,
            child: Container(
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 67, 101, 1),
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(Icons.arrow_back_sharp, size: 19.sp),
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 4.h,
                    width: double.infinity,
                    margin: EdgeInsets.fromLTRB(6.w, 5.h, 6.w, 0.h),
                    child: Text(
                      "Make your Bio-Data",
                      style: TextStyle(
                          fontSize: 20.sp,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Com"),
                    ),
                  ),
                  Container(
                    height: 4.h,
                    width: double.infinity,
                    margin:
                        EdgeInsets.symmetric(horizontal: 6.w, vertical: 1.5.h),
                    child: Text(
                      "Enter Your Peronal Details Here.",
                      style: TextStyle(fontSize: 18.sp, fontFamily: "Com"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    margin: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15.sp)),
                    child: Column(
                      children: [
                        Container(
                          height: 4.h,
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          width: double.infinity,
                          margin: EdgeInsets.fromLTRB(0.w, 3.h, 0.w, 0.h),
                          child: Text(
                            "Personal Details",
                            style: TextStyle(
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Com"),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // birth date

                                TextFormField(
                                  controller: dob,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    dob.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter DOB",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " DOB",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "DOB cannot be Empty";
                                    }
                                    return null;
                                  },
                                ),

                                // dob place
                                TextFormField(
                                  controller: dob_place,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    dob_place.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter DOB Place",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " DOB Place",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "DOB Place cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "DOB Place length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // caste

                                TextFormField(
                                  controller: caste,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    caste.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Caste",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Caste",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Caste cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Caste length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // material status

                                TextFormField(
                                  controller: maternal_status,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    maternal_status.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Maternal Status",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Maternal Status",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Maternal Status cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Maternal Status length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // education

                                TextFormField(
                                  controller: education,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    education.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Education/Qualification",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Education/Qualification",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Education/Qualification cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Education/Qualification length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // occupasion

                                TextFormField(
                                  controller: occupasion,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    occupasion.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Occupasion",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Occupasion",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Occupasion cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Occupasion length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // job

                                TextFormField(
                                  controller: job,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    job.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Job/Business",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Job/Business",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Job/Business cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Job/Business length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // salary

                                TextFormField(
                                  controller: salary,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    salary.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Salary",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Salary",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Salary cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Salary length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // address

                                TextFormField(
                                  controller: address,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    address.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Address",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Address",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Address cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Address length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // next page button

                  // Container(
                  //   height: 8.h,
                  //   width: double.infinity,
                  //   margin: EdgeInsets.fromLTRB(5.w, 0, 5.w, 3.h),
                  //   child: RoundedLoadingButton(
                  //       valueColor: Colors.black,
                  //       borderRadius: 12,
                  //       width: double.infinity,
                  //       color: Colors.white,
                  //       controller: btn1,
                  //       successColor: Colors.green,
                  //       errorColor: Colors.red,
                  //       successIcon: Icons.check,
                  //       onPressed: () {
                  //         // Navigator.pushNamed(context, Myroutes.family_details);
                  //         // Timer(Duration(seconds: 2), () {
                  //         //   btn1.reset();
                  //         // });
                  //         if (_formKey.currentState!.validate()) {
                  //           Navigator.pushNamed(
                  //               context, Myroutes.family_details);
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         } else {
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         }
                  //       },
                  //       child: Icon(
                  //         Icons.arrow_right_alt,
                  //         color: Colors.black,
                  //         shadows: [Shadow(color: Colors.black)],
                  //         size: 25.sp,
                  //       )),
                  // )

                  InkWell(
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        family_details(
                          name: name.text,
                          dob: dob.text,
                          dob_place: dob_place.text,
                          caste: caste.text,
                          maternal_status: maternal_status.text,
                          education: education.text,
                          occupasion: occupasion.text,
                          job: job.text,
                          salary: salary.text,
                          address: address.text,
                        );
                        // btn change logic
                        //getUser();
                        Timer(Duration(milliseconds: 500), () {
                          setState(() {
                            changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });

                        Future.delayed(
                          Duration(seconds: 2),
                          () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return family_details(
                                name: name.text,
                                dob: dob.text,
                                dob_place: dob_place.text,
                                caste: caste.text,
                                maternal_status: maternal_status.text,
                                education: education.text,
                                occupasion: occupasion.text,
                                job: job.text,
                                salary: salary.text,
                                address: address.text,
                              );
                            }));
                          },
                        );
                      } else {
                        Timer(Duration(milliseconds: 100), () {
                          setState(() {
                            nested_changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text(
                            'Enter Valid Data !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontFamily: "Com"),
                          )),
                        );
                      }
                    },
                    child: Container(
                        alignment: Alignment.center,
                        height: 7.h,
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(5.w, 1.h, 5.w, 4.h),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.sp),
                        ),
                        child: changebtn
                            ? Icon(
                                Icons.done,
                                size: 5.h,
                              )
                            : nested_changebtn
                                ? Icon(
                                    Icons.close,
                                    size: 5.h,
                                  )
                                : Icon(
                                    Icons.arrow_right_alt,
                                    size: 5.h,
                                  )),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

// apio call in retrofit
// Future<void> getUser() async {
//   print("Fetch user called!");
//   final dio = Dio();
//   final client = RestClient(dio);
//   await client
//       .addUser(
//           name.text,
//           dob.text,
//           dob_place.text,
//           caste.text,
//           maternal_status.text,
//           education.text,
//           occupasion.text,
//           job.text,
//           salary.text,
//           address.text,
//           "father_name",
//           "mother_name",
//           "sister_name",
//           "brother_name",
//           "grandfather_name",
//           "sistegrandmother_namer_name",
//           "mamaji_name",
//           "nanaji_name",
//           "contact_number",
//           "whatsapp_number",
//           "email")
//       .then(
//     (value) {
//       print("RESPONCE:: $value");
//     },
//   );
// }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:vivah/signup_pages/contact_details.dart';
import 'package:vivah/utils/routes.dart';

class maternal_details extends StatefulWidget {
  String? name;
  String? dob;
  String? dob_place;
  String? caste;
  String? maternal_status;
  String? education;
  String? occupasion;
  String? job;
  String? salary;
  String? address;
  String? father_name;
  String? mother_name;
  String? sister_name;
  String? brother_name;
  String? grandfather_name;
  String? grandmother_name;

  maternal_details(
      {Key? key,
      required this.name,
      required this.dob,
      required this.dob_place,
      required this.caste,
      required this.maternal_status,
      required this.education,
      required this.occupasion,
      required this.job,
      required this.salary,
      required this.address,
      required this.father_name,
      required this.mother_name,
      required this.sister_name,
      required this.brother_name,
      required this.grandfather_name,
      required this.grandmother_name})
      : super(key: key);

  @override
  State<maternal_details> createState() => _maternal_details();
}

class _maternal_details extends State<maternal_details> {
  final _formKey = GlobalKey<FormState>();
  final mamaji_name = new TextEditingController();
  final nanaji_name = new TextEditingController();
  final naniji_name = new TextEditingController();

  final RoundedLoadingButtonController btn1 = RoundedLoadingButtonController();

  bool changebtn = false;
  bool nested_changebtn = false;

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            radius: 100,
            child: Container(
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 67, 101, 1),
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(Icons.arrow_back_sharp, size: 19.sp),
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 4.h,
                    width: double.infinity,
                    margin:
                        EdgeInsets.symmetric(horizontal: 6.w, vertical: 1.5.h),
                    child: Text(
                      "Enter Your Maternal Details Here.",
                      style: TextStyle(fontSize: 18.sp, fontFamily: "Com"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    margin: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15.sp)),
                    child: Column(
                      children: [
                        Container(
                          height: 4.h,
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          width: double.infinity,
                          margin: EdgeInsets.fromLTRB(0.w, 3.h, 0.w, 0.h),
                          child: Text(
                            "Maternal Details",
                            style: TextStyle(
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Com"),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: mamaji_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    mamaji_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Mamaji's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Mamaji's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Mamaji's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Mamaji's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // birth date

                                TextFormField(
                                  controller: nanaji_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    nanaji_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Nanaji's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Nanaji's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Nanaji's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Nanaji's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // dob place
                                TextFormField(
                                  controller: naniji_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    naniji_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Naniji's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: "  Naniji's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Naniji's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Naniji's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // next page button

                  // Container(
                  //   height: 8.h,
                  //   width: double.infinity,
                  //   margin: EdgeInsets.fromLTRB(5.w, 0, 5.w, 3.h),
                  //   child: RoundedLoadingButton(
                  //       valueColor: Colors.black,
                  //       borderRadius: 12,
                  //       width: double.infinity,
                  //       color: Colors.white,
                  //       controller: btn1,
                  //       successColor: Colors.green,
                  //       errorColor: Colors.red,
                  //       successIcon: Icons.check,
                  //       onPressed: () {
                  //         // Navigator.pushNamed(context, Myroutes.family_details);
                  //         // Timer(Duration(seconds: 2), () {
                  //         //   btn1.reset();
                  //         // });
                  //         if (_formKey.currentState!.validate()) {
                  //           Navigator.pushNamed(
                  //               context, Myroutes.family_details);
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         } else {
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         }
                  //       },
                  //       child: Icon(
                  //         Icons.arrow_right_alt,
                  //         color: Colors.black,
                  //         shadows: [Shadow(color: Colors.black)],
                  //         size: 25.sp,
                  //       )),
                  // )

                  InkWell(
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        // btn change logic

                        Timer(Duration(milliseconds: 500), () {
                          setState(() {
                            changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });

                        Future.delayed(
                          Duration(seconds: 2),
                          () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return contact_details(
                                name: widget.name,
                                dob: widget.dob,
                                dob_place: widget.dob_place,
                                caste: widget.caste,
                                maternal_status: widget.maternal_status,
                                education: widget.education,
                                occupasion: widget.occupasion,
                                job: widget.job,
                                salary: widget.salary,
                                address: widget.address,
                                father_name: widget.father_name,
                                mother_name: widget.mother_name,
                                sister_name: widget.sister_name,
                                brother_name: widget.brother_name,
                                grandfather_name: widget.grandfather_name,
                                grandmother_name: widget.grandmother_name,
                                mamaji_name: mamaji_name.text,
                                nanaji_name: nanaji_name.text,
                                naniji_name: naniji_name.text,
                              );
                            }));
                          },
                        );
                      } else {
                        Timer(Duration(milliseconds: 100), () {
                          setState(() {
                            nested_changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text(
                            'Enter Valid Data !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontFamily: "Com"),
                          )),
                        );
                      }
                    },
                    child: Container(
                        alignment: Alignment.center,
                        height: 7.h,
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(5.w, 1.h, 5.w, 4.h),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.sp),
                        ),
                        child: changebtn
                            ? Icon(
                                Icons.done,
                                size: 5.h,
                              )
                            : nested_changebtn
                                ? Icon(
                                    Icons.close,
                                    size: 5.h,
                                  )
                                : Icon(
                                    Icons.arrow_right_alt,
                                    size: 5.h,
                                  )),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}

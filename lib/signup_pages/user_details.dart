class user_details {
  String? name;
  String? dob;
  String? dob_place;
  String? caste;
  String? maternal_status;
  String? education;
  String? occupasion;
  String? job;
  String? salary;
  String? address;

  user_details({
    this.name,
    this.dob,
    this.dob_place,
    this.caste,
    this.maternal_status,
    this.education,
    this.occupasion,
    this.job,
    this.salary,
    this.address,
  });

  user_details.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    dob = json["dob"];
    dob_place = json["dob_place"];
    caste = json["caste"];
    maternal_status = json["maternal_status"];
    education = json["education"];
    occupasion = json["occupasion"];
    job = json["job"];
    salary = json["salary"];
    address = json["address"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['dob'] = this.dob;
    data['dob_place'] = this.dob_place;
    data['caste'] = this.caste;
    data['maternal_status'] = this.maternal_status;
    data['education'] = this.education;
    data['occupasion'] = this.occupasion;
    data['job'] = this.job;
    data['salary'] = this.salary;
    data['address'] = this.address;

    return data;
  }
}

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:vivah/api/rest_client2.dart';
import 'package:vivah/signup_pages/maternal_details.dart';
import 'package:vivah/utils/routes.dart';

class family_details extends StatefulWidget {
  String? name;
  String? dob;
  String? dob_place;
  String? caste;
  String? maternal_status;
  String? education;
  String? occupasion;
  String? job;
  String? salary;
  String? address;

  family_details(
      {Key? key,
      required this.name,
      required this.dob,
      required this.dob_place,
      required this.caste,
      required this.maternal_status,
      required this.education,
      required this.occupasion,
      required this.job,
      required this.salary,
      required this.address})
      : super(key: key);

  @override
  State<family_details> createState() => _family_details(
      // name: name!,
      // dob: dob!,
      // dob_place: dob_place!,
      // caste: caste!,
      // maternal_status: maternal_status!,
      // education: education!,
      // occupasion: occupasion!,
      // job: job!,
      // salary: salary!,
      // address: address!
      );
}

class _family_details extends State<family_details> {
  final _formKey = GlobalKey<FormState>();
  final father_name = new TextEditingController();
  final mother_name = new TextEditingController();
  final sister_name = new TextEditingController();
  final brother_name = new TextEditingController();
  final grandfather_name = new TextEditingController();
  final grandmother_name = new TextEditingController();

  final RoundedLoadingButtonController btn1 = RoundedLoadingButtonController();

  bool changebtn = false;
  bool nested_changebtn = false;

  //  String name;
  // String dob;
  // String dob_place;
  // String caste;
  // String maternal_status;
  // String education;
  // String occupasion;
  // String job;
  // String salary;
  // String address;
  //  // _family_details(
  //   //     {required this.name,
  //   //     required this.dob,
  //   //     required this.dob_place,
  //   //     required this.caste,
  //   //     required this.maternal_status,
  //   //     required this.education,
  //   //     required this.occupasion,
  //   //     required this.job,
  //   //     required this.salary,
  //   //     required this.address});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return Scaffold(
        extendBody: true,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            radius: 100,
            child: Container(
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 67, 101, 1),
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(Icons.arrow_back_sharp, size: 19.sp),
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: 4.h,
                    width: double.infinity,
                    margin:
                        EdgeInsets.symmetric(horizontal: 6.w, vertical: 1.5.h),
                    child: Text(
                      "Enter Your Family Details Here.",
                      style: TextStyle(fontSize: 18.sp, fontFamily: "Com"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    margin: EdgeInsets.fromLTRB(5.w, 0.h, 5.w, 3.h),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15.sp)),
                    child: Column(
                      children: [
                        Container(
                          height: 4.h,
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          width: double.infinity,
                          margin: EdgeInsets.fromLTRB(0.w, 3.h, 0.w, 0.h),
                          child: Text(
                            "Family Details",
                            style: TextStyle(
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Com"),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                // father name

                                TextFormField(
                                  controller: father_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    father_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Father's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Father's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Father's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Father's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // birth date

                                TextFormField(
                                  controller: mother_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    mother_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Mother's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Mother's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Mother's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Mother's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // dob place
                                TextFormField(
                                  controller: sister_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    sister_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Sister's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: "  Sister's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Sister's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Sister's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // caste

                                TextFormField(
                                  controller: brother_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    brother_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Brother's name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Brother's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Brother's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Brother's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // material status

                                TextFormField(
                                  controller: grandfather_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    grandfather_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Grand Father's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Grand Father's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Grand Father's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Grand Father's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),

                                // education

                                TextFormField(
                                  controller: grandmother_name,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    grandmother_name.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontFamily: "Com",
                                    fontSize: 17.5.sp,
                                  ),
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.pinkAccent,
                                      fontFamily: "Com",
                                    ),
                                    hintText: "Enter Grand Mother's Name",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        height: 0.2.h,
                                        color: Colors.grey,
                                        fontFamily: "Comfortaa",
                                        fontSize: 15.sp),
                                    labelText: " Grand Mother's Name",
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontFamily: "Comfortaa",
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Grand Mother's Name cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 3) {
                                      return "Grand Mother's Name length should be atleast 3";
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // next page button

                  // Container(
                  //   height: 8.h,
                  //   width: double.infinity,
                  //   margin: EdgeInsets.fromLTRB(5.w, 0, 5.w, 3.h),
                  //   child: RoundedLoadingButton(
                  //       valueColor: Colors.black,
                  //       borderRadius: 12,
                  //       width: double.infinity,
                  //       color: Colors.white,
                  //       controller: btn1,
                  //       successColor: Colors.green,
                  //       errorColor: Colors.red,
                  //       successIcon: Icons.check,
                  //       onPressed: () {
                  //         // Navigator.pushNamed(context, Myroutes.family_details);
                  //         // Timer(Duration(seconds: 2), () {
                  //         //   btn1.reset();
                  //         // });
                  //         if (_formKey.currentState!.validate()) {
                  //           Navigator.pushNamed(
                  //               context, Myroutes.family_details);
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         } else {
                  //           Timer(Duration(seconds: 2), () {
                  //             btn1.reset();
                  //           });
                  //         }
                  //       },
                  //       child: Icon(
                  //         Icons.arrow_right_alt,
                  //         color: Colors.black,
                  //         shadows: [Shadow(color: Colors.black)],
                  //         size: 25.sp,
                  //       )),
                  // )

                  InkWell(
                    onTap: () {
                      if (_formKey.currentState!.validate()) {
                        // getUser();
                        // btn change logic

                        Timer(Duration(milliseconds: 500), () {
                          setState(() {
                            changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });

                        Future.delayed(
                          Duration(seconds: 2),
                          () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return maternal_details(
                                name: widget.name,
                                dob: widget.dob,
                                dob_place: widget.dob_place,
                                caste: widget.caste,
                                maternal_status: widget.maternal_status,
                                education: widget.education,
                                occupasion: widget.occupasion,
                                job: widget.job,
                                salary: widget.salary,
                                address: widget.address,
                                father_name: father_name.text,
                                mother_name: mother_name.text,
                                sister_name: sister_name.text,
                                brother_name: brother_name.text,
                                grandfather_name: grandfather_name.text,
                                grandmother_name: grandmother_name.text,
                              );
                            }));
                          },
                        );
                      } else {
                        Timer(Duration(milliseconds: 100), () {
                          setState(() {
                            nested_changebtn = true;
                          });
                        });
                        Timer(Duration(seconds: 2), () {
                          setState(() {
                            changebtn = false;
                            nested_changebtn = false;
                          });
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text(
                            'Enter Valid Data !',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontFamily: "Com"),
                          )),
                        );
                      }
                    },
                    child: Container(
                        alignment: Alignment.center,
                        height: 7.h,
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(5.w, 1.h, 5.w, 4.h),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15.sp),
                        ),
                        child: changebtn
                            ? Icon(
                                Icons.done,
                                size: 5.h,
                              )
                            : nested_changebtn
                                ? Icon(
                                    Icons.close,
                                    size: 5.h,
                                  )
                                : Icon(
                                    Icons.arrow_right_alt,
                                    size: 5.h,
                                  )),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

// apio call in retrofit
// Future<void> getUser() async {
//   print("Fetch user called!");
//   final dio = Dio();
//   final client = RestClient(dio);
//   await client
//       .addUser(
//           'name.text',
//           'dob.text',
//           'dob_place.text',
//           'caste.text',
//           'maternal_status.text',
//           'education.text',
//           'occupasion.text',
//           'job.text',
//           'salary.text',
//           'address.text',
//           father_name.text,
//           mother_name.text,
//           sister_name.text,
//           brother_name.text,
//           grandfather_name.text,
//           grandmother_name.text,
//           "mamaji_name",
//           "nanaji_name",
//           "contact_number",
//           "whatsapp_number",
//           "email")
//       .then(
//     (value) {
//       print("RESPONCE:: $value");
//     },
//   );
// }
}

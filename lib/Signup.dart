import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:vivah/utils/routes.dart';
import 'package:vivah/utils/showSnakbar.dart';
import 'package:vivah/utils/usermodel.dart';

import 'package:responsive_sizer/responsive_sizer.dart';

class Signup extends StatefulWidget {
  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final _auth = FirebaseAuth.instance;
  String username = "";
  final _formKey = GlobalKey<FormState>();
  bool changebtn = false;
  bool nested_changebtn = false;
  int container_size = 400;
  final firstNameEditingController = new TextEditingController();
  final lastNameEditingController = new TextEditingController();
  final emailEditingController = new TextEditingController();
  final passwordEditingController = new TextEditingController();
  final confirmPasswordEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, screenType) {
        return Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent.withOpacity(0),
            elevation: 0,
            leading: IconButton(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                color: Colors.black,
                onPressed: () {
                  Navigator.pushNamed(context, Myroutes.homeRoute);
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                )),
          ),
          body: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                "assets/img/signup_bg.jpg",
                fit: BoxFit.cover,
              ),
              Center(
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 6.w),
                    child: Column(
                      children: [
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Hey ! there,",
                            style: TextStyle(
                                fontSize: 23.sp,
                                fontFamily: "Comfortaa",
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Row(
                          children: [
                            Text(
                              "Signup, ",
                              style: TextStyle(
                                  fontSize: 23.sp,
                                  fontFamily: "Comfortaa",
                                  fontWeight: FontWeight.w900),
                            ),
                            Text(
                              "and find your",
                              style: TextStyle(
                                  fontSize: 20.sp, fontFamily: "Comfortaa"),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Soulmate.",
                            style: TextStyle(
                                fontSize: 28.sp,
                                fontWeight: FontWeight.w900,
                                fontFamily: "Comfortaa"),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 2.h),
                          padding: EdgeInsets.symmetric(
                              vertical: 1.h, horizontal: 4.w),
                          //height: MediaQuery.of(context).size.height*0.6,
                          height: 45.h,

                          width: MediaQuery.of(context).size.width * 0.9,
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.9),
                              borderRadius: BorderRadius.circular(15.sp)),
                          child: SingleChildScrollView(
                            child: Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin:
                                            EdgeInsets.fromLTRB(0, 0, 1.w, 0),
                                        child:
                                            Image.asset("assets/img/user.png"),
                                      )),
                                      Expanded(
                                        child: TextFormField(
                                          controller:
                                              firstNameEditingController,
                                          keyboardType: TextInputType.name,
                                          onSaved: (newValue) {
                                            firstNameEditingController.text =
                                                newValue!;
                                          },
                                          textInputAction: TextInputAction.next,
                                          style: TextStyle(
                                            fontFamily: "Com",
                                            fontSize: 17.5.sp,
                                          ),
                                          cursorColor: Colors.black,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                1.w, 2.h, 1.w, 1.h),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.black)),
                                            floatingLabelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w800,
                                              color: Color.fromRGBO(
                                                  255, 67, 101, 1),
                                              fontFamily: "Com",
                                            ),
                                            hintText: "Enter First Name",
                                            hintStyle: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                height: 0.2.h,
                                                color: Colors.grey,
                                                fontFamily: "Comfortaa",
                                                fontSize: 15.sp),
                                            labelText: "  First Name",
                                            labelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontFamily: "Comfortaa",
                                            ),
                                          ),
                                          onChanged: (value) {
                                            username = value;
                                            setState(() {});
                                          },
                                          validator: (value) {
                                            if (value != null &&
                                                value.isEmpty) {
                                              return "First Name cannot be Empty";
                                            } else if (value != null &&
                                                value.length < 3) {
                                              return "First Name length should be atleast 3";
                                            }
                                            return null;
                                          },
                                        ),
                                        flex: 13,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin:
                                            EdgeInsets.fromLTRB(0, 0, 1.w, 0),
                                        child:
                                            Image.asset("assets/img/user.png"),
                                      )),
                                      Expanded(
                                        child: TextFormField(
                                          controller: lastNameEditingController,
                                          keyboardType: TextInputType.name,
                                          onSaved: (newValue) {
                                            lastNameEditingController.text =
                                                newValue!;
                                          },
                                          textInputAction: TextInputAction.next,
                                          style: TextStyle(
                                            fontFamily: "Com",
                                            fontSize: 17.5.sp,
                                          ),
                                          cursorColor: Colors.black,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                1.w, 2.h, 1.w, 1.h),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.black)),
                                            floatingLabelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w800,
                                              color: Color.fromRGBO(
                                                  255, 67, 101, 1),
                                              fontFamily: "Com",
                                            ),
                                            hintText: "Enter Last Name",
                                            hintStyle: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                height: 0.2.h,
                                                color: Colors.grey,
                                                fontFamily: "Comfortaa",
                                                fontSize: 15.sp),
                                            labelText: "  Last Name",
                                            labelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontFamily: "Comfortaa",
                                            ),
                                          ),
                                          onChanged: (value) {
                                            username = value;
                                            setState(() {});
                                          },
                                          validator: (value) {
                                            if (value != null &&
                                                value.isEmpty) {
                                              return "Last Name cannot be Empty";
                                            } else if (value != null &&
                                                value.length < 3) {
                                              return "First Name length should be atleast 3";
                                            }
                                            return null;
                                          },
                                        ),
                                        flex: 13,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin: EdgeInsets.fromLTRB(0, 0, 2, 0),
                                        child: Image.asset(
                                            "assets/img/mail_icon.png"),
                                      )),
                                      Expanded(
                                        child: TextFormField(
                                          controller: emailEditingController,
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          onSaved: (newValue) {
                                            emailEditingController.text =
                                                newValue!;
                                          },
                                          textInputAction: TextInputAction.next,
                                          style: TextStyle(
                                            fontFamily: "Com",
                                            fontSize: 17.5.sp,
                                          ),
                                          cursorColor: Colors.black,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                1.w, 2.h, 1.w, 1.h),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.black)),
                                            floatingLabelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w800,
                                              color: Color.fromRGBO(
                                                  255, 67, 101, 1),
                                              fontFamily: "Com",
                                            ),
                                            hintText: "Enter Email-ID",
                                            hintStyle: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                height: 0.2.h,
                                                color: Colors.grey,
                                                fontFamily: "Comfortaa",
                                                fontSize: 15.sp),
                                            labelText: "  Email",
                                            labelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontFamily: "Comfortaa",
                                            ),
                                          ),
                                          onChanged: (value) {
                                            username = value;
                                            setState(() {});
                                          },
                                          validator: (value) {
                                            if (value != null &&
                                                value.isEmpty) {
                                              return "Email cannot be Empty";
                                            } else if (value != null &&
                                                value.length < 6) {
                                              return "Username length should be atleast 6";
                                            }
                                            return null;
                                          },
                                        ),
                                        flex: 13,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin: EdgeInsets.fromLTRB(0, 0, 3, 0),
                                        child: Image.asset(
                                            "assets/img/open_pass_icon.png"),
                                      )),
                                      Expanded(
                                        child: TextFormField(
                                          controller: passwordEditingController,
                                          keyboardType:
                                              TextInputType.visiblePassword,
                                          onSaved: (newValue) {
                                            passwordEditingController.text =
                                                newValue!;
                                          },
                                          textInputAction: TextInputAction.next,
                                          style: TextStyle(
                                            fontFamily: "Com",
                                            fontSize: 17.5.sp,
                                          ),
                                          cursorColor: Colors.black,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                1.w, 2.h, 1.w, 1.h),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.black)),
                                            floatingLabelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w800,
                                              color: Color.fromRGBO(
                                                  255, 67, 101, 1),
                                              fontFamily: "Com",
                                            ),
                                            hintText: "Enter Password",
                                            hintStyle: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                height: 0.2.h,
                                                color: Colors.grey,
                                                fontFamily: "Comfortaa",
                                                fontSize: 15.sp),
                                            labelText: "  Password",
                                            labelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontFamily: "Comfortaa",
                                            ),
                                          ),
                                          onChanged: (value) {
                                            username = value;
                                            setState(() {});
                                          },
                                          validator: (value) {
                                            if (value != null &&
                                                value.isEmpty) {
                                              return "Password cannot be Empty";
                                            } else if (value != null &&
                                                value.length < 8) {
                                              return "Password length should be atleast 8";
                                            }
                                            return null;
                                          },
                                        ),
                                        flex: 13,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                        margin: EdgeInsets.fromLTRB(0, 0, 3, 0),
                                        child: Image.asset(
                                            "assets/img/padlock.png"),
                                      )),
                                      Expanded(
                                        child: TextFormField(
                                          controller:
                                              confirmPasswordEditingController,
                                          onSaved: (newValue) {
                                            confirmPasswordEditingController
                                                .text = newValue!;
                                          },
                                          textInputAction: TextInputAction.done,
                                          obscureText: true,
                                          style: TextStyle(
                                            fontFamily: "Com",
                                            fontSize: 17.5.sp,
                                          ),
                                          cursorColor: Colors.black,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                1.w, 2.h, 1.w, 1.h),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.black)),
                                            floatingLabelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w800,
                                              color: Color.fromRGBO(
                                                  255, 67, 101, 1),
                                              fontFamily: "Com",
                                            ),
                                            hintText: "Re-enter Password",
                                            hintStyle: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                height: 0.2.h,
                                                color: Colors.grey,
                                                fontFamily: "Comfortaa",
                                                fontSize: 15.sp),
                                            labelText: "  Confirm Password",
                                            labelStyle: TextStyle(
                                              fontSize: 16.sp,
                                              color: Colors.black,
                                              fontFamily: "Comfortaa",
                                            ),
                                          ),
                                          onChanged: (value) {
                                            username = value;
                                            setState(() {});
                                          },
                                          validator: (value) {
                                            if (value != null &&
                                                value.isEmpty) {
                                              return "Confirm Password cannot be Empty";
                                            } else if (passwordEditingController
                                                    .text !=
                                                confirmPasswordEditingController
                                                    .text) {
                                              return "Password doesn't match !";
                                            }
                                            return null;
                                          },
                                        ),
                                        flex: 13,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () => signup(emailEditingController.text,
                              passwordEditingController.text),
                          child: Container(
                            height: 7.h,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 67, 101, 1),
                                borderRadius:
                                    BorderRadiusDirectional.circular(20.sp)),
                            child: changebtn
                                ? Icon(
                                    Icons.done,
                                    color: Colors.black,
                                  )
                                : nested_changebtn
                                    ? Icon(
                                        Icons.close,
                                        color: Colors.black,
                                      )
                                    : Center(
                                        child: Text(
                                          "Sign Up",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w900,
                                              fontFamily: "Comfortaa",
                                              fontSize: 18.sp),
                                        ),
                                      ),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            "by doing Siging up u accept our ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: "Com",
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {},
                          child: Text(
                            "Terms And Conditions",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold,
                              color: Color.fromRGBO(255, 67, 101, 1),
                              fontFamily: "Com",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void signup(String email, String password) async {
    if (_formKey.currentState!.validate()) {
      // btn change logic

      Timer(Duration(milliseconds: 500), () {
        setState(() {
          changebtn = true;
        });
      });
      Timer(Duration(seconds: 2), () {
        setState(() {
          changebtn = false;
          nested_changebtn = false;
        });
      });
      // try {
      //   _auth.currentUser!.sendEmailVerification();
      //   showSnackBar(context, "Email Verification sent !");
      // } on FirebaseAuthException catch (e) {
      //   showSnackBar(context, e.message!);
      // }
      print("ok      1");
      await _auth
          .createUserWithEmailAndPassword(email: email!, password: password!)
          .then((value) => {postDetailsToFirestore()})
          .then((value) => Future.delayed(
                Duration(seconds: 2),
                () {
                  try {
                    _auth.currentUser!.sendEmailVerification();
                    showSnackBar(context, "Email Verification sent !");
                  } on FirebaseAuthException catch (e) {
                    showSnackBar(context, e.message!);
                  }
                  Navigator.pushNamed(context, Myroutes.personal_details);
                },
              ))
          .catchError((e) {
        showSnackBar(context, e.message!);
        Timer(Duration(milliseconds: 100), () {
          setState(() {
            changebtn = false;
            nested_changebtn = true;
          });
        });
        Timer(Duration(seconds: 2), () {
          setState(() {
            changebtn = false;
            nested_changebtn = false;
          });
        });
      });
      await Future.delayed(Duration(seconds: 2));
    }
  }

  postDetailsToFirestore() async {
    //calling our firestore
    //calling our user model
    //sending this values

    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    usermodel? usermodal = usermodel();

    //write alll the values
    usermodal.uid = user!.uid;
    usermodal.email = user!.email;
    usermodal.firstname = firstNameEditingController.text!;
    usermodal.lastname = lastNameEditingController.text!;

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(usermodal.toMap());
    //Fluttertoast.showToast(msg: "Account Created Succesfully");
    showSnackBar(context, "Account Created Succesfully");
    setState(() {
      changebtn = false;
      nested_changebtn = false;
    });
  }
}

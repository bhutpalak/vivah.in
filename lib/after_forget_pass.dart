import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vivah/Login.dart';

import 'Vivah_home_page.dart';

class after_forgot_pass extends StatefulWidget {
  @override
  State<after_forgot_pass> createState() => _after_forgot_pass();
}

class _after_forgot_pass extends State<after_forgot_pass> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Login(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 67, 101, 1),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        shadowColor: Colors.black,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Forgot Password",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    fontFamily: "Comfortaa"),
                textAlign: TextAlign.left),
          ],
        )),
      ),
      body: Container(
        color: Color.fromRGBO(255, 67, 101, 1),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 250),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 25),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Center(
                  child: Column(
                    children: [
                      Text(
                        "Password Reset link send to your\nRegistred email-id",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16, fontFamily: "Com"),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

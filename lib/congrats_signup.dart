import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vivah/Login.dart';

import 'Vivah_home_page.dart';

class congrats_signup extends StatefulWidget {
  @override
  State<congrats_signup> createState() => _congrats_signup();
}

class _congrats_signup extends State<congrats_signup> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Login(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 67, 101, 1),
      body: Container(
        color: Color.fromRGBO(255, 67, 101, 1),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 40),
              margin: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Congratulations !",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Com"),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      "Your account Details Was Succesfully Submitted.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20, fontFamily: "Com"),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

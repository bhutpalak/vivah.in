import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//import 'package:vivah/Google_signup/google_signup.dart';
import 'package:vivah/Main_home.dart';
import 'package:vivah/after_forget_pass.dart';
import 'package:vivah/api/rest_client2.dart';
import 'package:vivah/congrats_signup.dart';
import 'package:vivah/forgot_pass.dart';
import 'package:vivah/pages/User_bio_data.dart';
import 'package:vivah/pages/User_chat.dart';
import 'package:vivah/pages/User_home.dart';
import 'package:vivah/pages/User_profile.dart';
import 'package:vivah/pages/User_search.dart';
import 'package:vivah/signup_pages/contact_details.dart';
import 'package:vivah/signup_pages/family_details.dart';
import 'package:vivah/signup_pages/maternal_details.dart';
import 'package:vivah/signup_pages/personal_details.dart';
import 'package:vivah/signup_pages/user_details.dart';
import 'package:vivah/utils/routes.dart';

import 'pages/Home_page_user.dart';
import 'Vivah_home_page.dart';
import 'First_page_vivah.dart';
import 'Login.dart';
import 'Signup.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyVivahapp());
}

class MyVivahapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: First_page_vivah(),
      routes: {
        Myroutes.homeRoute: (context) => Vivah_home_page(),
        Myroutes.loginRoute: (context) => Login(),
        Myroutes.signupRoute: (context) => Signup(),
        //Myroutes.user_homeRoute: (context) => User_home(),
        Myroutes.user_homeRoute: (context) => Home_page_user(),
        Myroutes.user_main_home: (context) => Main_page(),
        Myroutes.user_chats: (context) => User_chat(),
        Myroutes.user_profile: (context) => User_profile(),
        Myroutes.user_search: (context) => User_search(),
        Myroutes.user_home: (context) => User_home(),
        Myroutes.user_bio_data: (context) => User_bio_data(),
        Myroutes.forgot_pass: (context) => forgot_pass(),
        Myroutes.after_forgot_pass: (context) => after_forgot_pass(),
        //Myroutes.google_signup: (context) => google_signup(),
        Myroutes.personal_details: (context) => personal_details(),
        // Myroutes.contact_details: (context) => contact_details(),
        //  Myroutes.family_details: (context) => family_details(),
        // Myroutes.maternal_details: (context) => maternal_details(),
        Myroutes.congrats_signup: (context) => congrats_signup(),
      },
    );
  }
}

// // workin method 2
// Future<String> getUser() async {
//   final dio = Dio();
//   final client = RestClient(dio);
//   String data = await client.getTasks();
//   print(data.toString());
//   return data;
// }

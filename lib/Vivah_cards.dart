

import 'package:flutter/material.dart';
import 'package:flutter_swipable/flutter_swipable.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:vivah/pages/User_bio_data.dart';
import 'package:vivah/utils/routes.dart';

class Vivah_cards extends StatelessWidget {
  dynamic users;

  Vivah_cards({this.users});

  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Swipable(
      onSwipeRight: (finalPosition) {},
      onSwipeDown: (finalPosition) {},
      onSwipeLeft: (finalPosition) {},
      onSwipeUp: (finalPosition) {},
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return User_bio_data(usersbiodata: users,);
          },));
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(20.sp)),
          margin: EdgeInsets.fromLTRB(4.w, 2.h, 4.w, 9.h),
          width: double.infinity,
          child: Stack(
            fit: StackFit.expand,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(20.0.sp)),
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Colors.transparent, Colors.black],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.7, 1]),
                  ),
                  child: Image.network(
                    "${users!['image']}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadiusDirectional.circular(20.sp),
                    gradient: LinearGradient(
                        colors: [Colors.transparent, Colors.black],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.7, 1])),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0, 5.w, 0),
                    alignment: Alignment.bottomLeft,
                    child: Row(
                      children: [
                        Text(
                          "${users!['first_name']} ",
                          style: TextStyle(
                              fontSize: 25.sp,
                              color: Colors.white,
                              fontFamily: "Comfortaa",
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 25),
                          child: Text(
                            "'${users!['age']} ",
                            style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.white,
                              fontFamily: "Comfortaa",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(5.w, 0, 5.w, 2.h),
                    alignment: Alignment.bottomLeft,
                    child: Text("${users!['about_user']}",
                        style: TextStyle(
                            fontSize: 18.sp,
                            fontFamily: "Comfortaa",
                            fontWeight: FontWeight.w400,
                            color: Colors.white)),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*child: Image.network(
"${users!['image']}",
fit: BoxFit.cover,

)*/

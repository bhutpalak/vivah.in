// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_client2.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps,no_leading_underscores_for_local_identifiers

class _RestClient implements RestClient {
  _RestClient(
    this._dio, {
    this.baseUrl,
  }) {
    baseUrl ??=
        'https://63bc18b3fa38d30d85bb7c6c.mockapi.io/api/user_data/user_data';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<String> getTasks() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<String>(_setStreamType<String>(Options(
      method: 'GET',
      headers: _headers,
      extra: _extra,
    )
        .compose(
          _dio.options,
          '/',
          queryParameters: queryParameters,
          data: _data,
        )
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data!;
    return value;
  }

  @override
  Future<String> addUser(
    name,
    dob,
    dob_place,
    caste,
    maternal_status,
    education,
    occupasion,
    job,
    salary,
    address,
    father_name,
    mother_name,
    sister_name,
    brother_name,
    grandfather_name,
    grandmother_name,
    mamaji_name,
    nanaji_name,
    naniji_name,
    contact_number,
    whatsapp_number,
    email,
  ) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = {
      'name': name,
      'dob': dob,
      'dob_place': dob_place,
      'caste': caste,
      'maternal_status': maternal_status,
      'education': education,
      'occupasion': occupasion,
      'job': job,
      'salary': salary,
      'address': address,
      'father_name': father_name,
      'mother_name': mother_name,
      'sister_name': sister_name,
      'brother_name': brother_name,
      'grandfather_name': grandfather_name,
      'grandmother_name': grandmother_name,
      'mamaji_name': mamaji_name,
      'nanaji_name': nanaji_name,
      'nanaji_name': naniji_name,
      'contact_number': contact_number,
      'whatsapp_number': whatsapp_number,
      'email': email,
    };
    final _result = await _dio.fetch<String>(_setStreamType<String>(Options(
      method: 'POST',
      headers: _headers,
      extra: _extra,
    )
        .compose(
          _dio.options,
          '/',
          queryParameters: queryParameters,
          data: _data,
        )
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data!;
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}

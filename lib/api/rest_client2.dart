import 'dart:convert';

// import 'package:demo_api/Model/LaptopModel.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'rest_client2.g.dart';

@RestApi(
    baseUrl:
        "https://63bc18b3fa38d30d85bb7c6c.mockapi.io/api/user_data/user_data")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/")
  Future<String> getTasks();

// @DELETE("/tasks/{id}")
// Future<void> deleteTask(@Path() String id);

  @POST("/")
  Future<String> addUser(
    @Field('name') name,
    @Field('dob') dob,
    @Field('dob_place') dob_place,
    @Field('caste') caste,
    @Field('material_status') maternal_status,
    @Field('education') education,
    @Field('occupasion') occupasion,
    @Field('job') job,
    @Field('salary') salary,
    @Field('address') address,
    @Field('father_name') father_name,
    @Field('mother_name') mother_name,
    @Field('sister_name') sister_name,
    @Field('brother_name') brother_name,
    @Field('grandfather_name') grandfather_name,
    @Field('grandmother_name') grandmother_name,
    @Field('mamaji_name') mamaji_name,
    @Field('nanaji_name') nanaji_name,
    @Field('naniji_name') naniji_name,
    @Field('contact_number') contact_number,
    @Field('whatsapp_number') whatsapp_number,
    @Field('email') email,
  );
}
// @JsonSerializable()
// class Post {
//   String? name;
//   String? dob;
//   String? dob_place;
//   String? caste;
//   String? maternal_status;
//   String? education;
//   String? occupasion;
//   String? job;
//   String? salary;
//   String? address;
//   String? father_name;
//   String? mother_name;
//   String? sister_name;
//   String? brother_name;
//   String? grandfather_name;
//   String? grandmother_name;
//   String? mamaji_name;
//   String? nanaji_name;
//   String? naniji_name;
//   String? contact_number;
//   String? whatsapp_number;
//   String? email;
//
//   Post.name(
//       {this.name,
//       this.dob,
//       this.dob_place,
//       this.caste,
//       this.maternal_status,
//       this.education,
//       this.occupasion,
//       this.job,
//       this.salary,
//       this.address,
//       this.father_name,
//       this.mother_name,
//       this.sister_name,
//       this.brother_name,
//       this.grandfather_name,
//       this.grandmother_name,
//       this.mamaji_name,
//       this.nanaji_name,
//       this.naniji_name,
//       this.contact_number,
//       this.whatsapp_number,
//       this.email});
//
//   factory post.fromJson(Map<String,dynamic json)=> _PostFromJson(json);
//   Map<String, dynamic> toJson()=>_PostToJson(this);
// }

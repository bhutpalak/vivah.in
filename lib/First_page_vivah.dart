import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:vivah/firebase_auth_methods.dart';

import 'Vivah_home_page.dart';

class First_page_vivah extends StatefulWidget {
  @override
  State<First_page_vivah> createState() => _First_page_vivahState();
}

class _First_page_vivahState extends State<First_page_vivah> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Vivah_home_page(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/img/logo_page.jpg",
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }
}

class usermodel {
  String? uid;
  String? firstname;
  String? lastname;
  String? email;
  String? password;

  usermodel({this.uid, this.email, this.firstname, this.lastname});

  factory usermodel.fromMap(map) {
    return usermodel(
        uid: map['uid'],
        email: map['email'],
        firstname: map['firstname'],
        lastname: map['lastname']);
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'firstname': firstname,
      'lastname': lastname,
    };
  }
}

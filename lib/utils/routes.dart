class Myroutes {
  static String loginRoute = "/login";
  static String homeRoute = "/home";
  static String signupRoute = "/signup";

  // ignore: non_constant_identifier_names
  static String user_homeRoute = "/user_home";
  static String user_main_home = "/main_home";
  static String user_search = "/user_search";
  static String user_chats = "/user_chats";
  static String user_profile = "/user_profile";
  static String user_home = "/user_home";
  static String user_bio_data = "/user_bio_data";
  static String forgot_pass = "/forgot_pass";
  static String after_forgot_pass = "/after_forgot_pass";
  static String google_signup = "/google_signup";
  static String personal_details = "/personal_details";
  static String family_details = "/family_details";
  static String maternal_details = "/maternal_details";
  static String contact_details = "/contact_details";
  static String congrats_signup = "/congrats_signup";
}

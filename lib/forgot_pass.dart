import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:vivah/utils/routes.dart';

class forgot_pass extends StatefulWidget {
  @override
  State<forgot_pass> createState() => _forgot_passState();
}

class _forgot_passState extends State<forgot_pass> {
  final _formKey = GlobalKey<FormState>();

  String forgot_mail = "";
  final TextEditingController forgot_mail_controller =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 67, 101, 1),
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        shadowColor: Colors.black,
        backgroundColor: Color.fromRGBO(255, 67, 101, 1),
        title: Container(
            margin: EdgeInsets.only(right: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Forgot Password",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: "Comfortaa"),
                    textAlign: TextAlign.left),
              ],
            )),
      ),
      body: Container(
        color: Color.fromRGBO(255, 67, 101, 1),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 250),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 35),
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Column(
                      children: [
                        Text(
                          "Receive an email in your registred mail to\nforgot your password",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 15, fontFamily: "Com"),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/img/mail_icon.png",
                                    alignment: Alignment.bottomCenter,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: forgot_mail_controller,
                                  keyboardType: TextInputType.name,
                                  onSaved: (newValue) {
                                    forgot_mail_controller.text = newValue!;
                                  },
                                  textInputAction: TextInputAction.next,
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                  cursorColor: Colors.grey,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(5, 20, 0, 10),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.black)),
                                    floatingLabelStyle: TextStyle(
                                      color: Colors.pinkAccent,
                                      fontSize: 20,
                                    ),
                                    hintText: "Enter Email",
                                    hintStyle: TextStyle(
                                        color: Colors.grey, fontSize: 15),
                                    labelText: "Email",
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 20),
                                  ),
                                  onChanged: (value) {
                                    forgot_mail = value;
                                    setState(() {});
                                  },
                                  // validator: (email) {
                                  //   email != null && !EmailValidator.validate(email)
                                  //       ? "Enter a valid email"
                                  //       : null;
                                  // },
                                  validator: (value) {
                                    if (value != null && value.isEmpty) {
                                      return "Email cannot be Empty";
                                    } else if (value != null &&
                                        value.length < 6) {
                                      return "Email length should be atleast 6";
                                    }
                                    return null;
                                  },
                                ),
                                flex: 10,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 67, 101, 1),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: InkWell(
                            onTap: () => resetPass(),
                            child: AnimatedContainer(
                              duration: Duration(seconds: 1),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.mail,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Reset Password",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future resetPass() async {
    // try {
    if (_formKey.currentState!.validate()) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(
            child: CircularProgressIndicator(
          color: Color.fromRGBO(255, 67, 101, 1),
        )),
      );
      await FirebaseAuth.instance
          .sendPasswordResetEmail(email: forgot_mail_controller.text);
      SnackBar(
        content: AwesomeSnackbarContent(
            title: "Password Reset Email Sent !",
            message: "",
            contentType: ContentType.success),
      );
      Navigator.pushNamed(context, Myroutes.after_forgot_pass);
      //Navigator.pushNamed(context, Myroutes.loginRoute);
      // } on FirebaseAuthException catch (e) {
      //   print(e);
      //   SnackBar(
      //     content: AwesomeSnackbarContent(
      //         title: "$e.message",
      //         message: "",
      //         contentType: ContentType.failure),
      //   );
      // }
    }
  }
}
